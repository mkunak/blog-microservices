import React from 'react'

import CreatePost from './components/CreatePost'
import PostList from './components/PostList'

const App = () => {
  return (
		<div className="container">
  		<h1 className="text-center my-4">Blog App</h1>
			{/* <hr /> */}
			<CreatePost title="Create your post" />
			<hr />
			<PostList title="My posts" />
		</div>
  )
}

export default App
