import React, { useState } from 'react'

/** services */
import { comments } from '../../services/requests'

const CreateComment = ({ title, postId }) => {
	const [formData, setFormData] = useState({ content: '' })

	const handleTextField = (e) => {
		setFormData({ ...formData, [e.target.name]: e.target.value})
	}

	const onSubmit = async (e) => {
		e.preventDefault()

		await comments.post(postId, formData)

		setFormData({ content: '' })
	}

	return (
		<div className="m-2">
			<form onSubmit={onSubmit}>
				<div className="form-group">
					<label htmlFor="create-comment-content">{title}</label>
					<textarea
					  id="create-comment-content"
						className="form-control"
						name="content"
						value={formData.content || ''}
						onChange={handleTextField}
					/>
				</div>
				<button className="btn btn-secondary my-4">Create</button>
			</form>
		</div>
	)
}

export default CreateComment
