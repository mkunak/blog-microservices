import React from 'react'

const CommentList = ({ title, commentList }) => {
	const comments = commentList.map((comment) => {
		const { id, status } = comment
		let { content } = comment

		if (status === 'pending') {
			content = 'Your message is awaiting moderation'
		}

		if (status === 'rejected') {
			content = 'Your message has been rejected'
		}

		return <li key={id}>{content}</li>
	})

	return (
		<div style={{ margin: '2rem' }}>
			<h3>{title}</h3>
			<ul>{comments}</ul>
		</div>
	)
}

export default CommentList
