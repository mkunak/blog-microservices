import React, { useState } from 'react'

/** services */
import { posts } from '../../services/requests'

const CreatePost = ({ title }) => {
	const [formData, setFormData] = useState({ title: '', content: '' })

	const handleTextField = (e) => {
		setFormData({ ...formData, [e.target.name]: e.target.value})
	}

	const onSubmit = async (e) => {
		e.preventDefault()

		await posts.post(formData)

		setFormData({ title: '', content: '' })
	}

	return (
		<div>
			<h3>{title}</h3>
			<form onSubmit={onSubmit}>
				<div className="form-group">
					<label htmlFor="create-post-title">Title</label>
					<input
					  id="create-post-title"
					  className="form-control"
						name="title"
						value={formData.title || ''}
						onChange={handleTextField}
					/>
				</div>
				<div className="form-group">
					<label htmlFor="create-post-content">Content</label>
					<textarea
					  id="create-post-content"
						className="form-control"
						name="content"
						value={formData.content || ''}
						onChange={handleTextField}
					/>
				</div>
				<button className="btn btn-primary my-4">Create</button>
			</form>
		</div>
	)
}

export default CreatePost
