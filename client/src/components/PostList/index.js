import React, { useEffect, useState } from 'react'

/** components */
import CommentList from '../CommentList'
import CreateComment from '../CreateComment'

/** services */
import { queries } from '../../services/requests'

const PostList = ({ title }) => {
	const [postList, setPostList] = useState([])

	async function fetchPosts() {
		const response = await queries.getPosts()
		console.log(response.data)
		setPostList(response.data)
	}

	useEffect(() => (fetchPosts()), [])

	return (
		<div>
			<h3>{title}</h3>
			<div className="d-flex flex-row flex-wrap justify-content-between">
				{postList.map((post) => {
					return (
						<div
						className="card"
						style={{ width: '40vw', marginBottom: '1rem' }}
						key={post.id}
						>
							<div className="card-body">
								<h4>{post.title}</h4>
								<p>{post.content}</p>
							</div>
							<CommentList title="Comments" commentList={post.comments} />
							<CreateComment title="Add comment" postId={post.id} />
						</div>
					)
				})}
			</div>
		</div>
	)
}

export default PostList
