import axios from 'axios'
import { config } from '../../config'

export const posts = {
	post: ({ title, content }) => {
		return axios.post(`${config.domain}/posts/create`, { title, content })
	},
}

export const comments = {
	post: (postId, { content }) => {
		return axios.post(`${config.domain}/comments/${postId}`, { content })
	},
}

export const queries = {
	getPosts: () => axios.get(`${config.domain}/posts`),
	getPostById: (postId) => axios.get(`${config.domain.queries}/posts/${postId}`),
}
