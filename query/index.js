import express from 'express'
import cors from 'cors'
import axios from 'axios'

const app = express()

app.use(express.json())
app.use(cors())

const posts = []

const handleEvent = (type, data) => {
  if (type === 'posts:item:created') {
    posts.push({ ...data, comments: [] })
  }

  if (type === 'comments:item:created') {
    const { postId, ...rest } = data
    const foundIndex = posts.findIndex((post) => post.id === postId)
    posts[foundIndex].comments.push(rest)
  }

  if (type === 'comments:item:updated') {
    const { id, postId, content, status } = data

    const foundPost = posts.find((post) => post.id === postId)
    const foundComment = foundPost.comments.find((comment) => comment.id === id)

    foundComment.content = content
    foundComment.status = status
  }
}

app.get('/posts', (_, res) => {
  res.send(posts)
})

app.get('/posts/:postId', (req, res) => {
  const { postId } = req.params
  res.send(posts.find((post) => post.id === postId))
})

app.post('/events', (req, res) => {
  const { type, data } = req.body

  handleEvent(type, data)

  res.send({})
})

const PORT = 4002
app.listen(PORT, async () => {
  console.log(`> App is up and running at port ${PORT}...`)

  const events = await axios.get('http://event-bus-srv:4005/events')

  events.data.forEach(event => {
    console.log('>>> Processing event:', event.type)

    handleEvent(event.type, event.data)
  })
})
