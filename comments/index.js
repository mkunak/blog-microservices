import express from 'express'
import crypto from 'crypto'
import cors from 'cors'
import axios from 'axios'

import { createRequire } from 'module' // Bring in the ability to create the 'require' method
const require = createRequire(import.meta.url); // construct the require method
const comments = require('./db/comments.json') // use the require method

const app = express()

app.use(express.json())
app.use(cors())

app.get('/comments', (_, res) => {
  res.send(comments)
})

app.get('/comments/:postId', (req, res) => {
  const { postId } = req.params

  const commentsByPostId = comments.filter((comment) => comment.postId === postId)

  res.send(commentsByPostId)
})

app.post('/comments/:postId', async (req, res) => {
  const { postId } = req.params
  const { content } = req.body
  const id = crypto.randomBytes(12).toString('hex')

  const newItem = { id, postId, content, status: 'pending' }
  comments.push(newItem)

  await axios.post('http://event-bus-srv:4005/events', {
    type: 'comments:item:created',
    data: newItem
  })

  res.status(201).send(newItem)
})

app.post('/events', async (req, res) => {
  console.log('>>> Receiving event:', req.body.type)

  const { type, data } = req.body

  if (type === 'comments:item:moderated') {
    const { id, postId, status } = data

    const commentsByPostId = comments.filter((comment) => comment.postId === postId)
    const foundComment = commentsByPostId.find((comment) => comment.id === id)

    foundComment.status = status

    console.log('> foundComment:', foundComment)

    await axios.post('http://event-bus-srv:4005/events', {
      type: 'comments:item:updated',
      data: foundComment,
    })
  }

  res.send({})
})

const PORT = 4001
app.listen(PORT, () => {
  console.log(`> App is up and running at port ${PORT}...`)
})
