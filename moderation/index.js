import express from 'express'
import axios from 'axios'

const app = express()

app.use(express.json())

app.get('/posts', (_, res) => {})

app.post('/events', async (req, res) => {
  console.log('>>> Receiving event:', req.body.type)
  const { type, data } = req.body

  if (type === 'comments:item:created') {
    const status = data.content.includes('orange') ? 'rejected' : 'approved'

    await axios.post('http://event-bus-srv:4005/events', {
      type: 'comments:item:moderated',
      data: { ...data, status },
    })
  }

  res.send({})
})

const PORT = 4003
app.listen(PORT, () => {
  console.log(`> App is up and running at port ${PORT}...`)
})
