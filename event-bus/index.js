import express from 'express'
import axios from 'axios'

const app = express()

app.use(express.json())

const events = []

app.post('/events', (req, res) => {
  console.log('>>> Receiving event:', req.body.type)
  const event = req.body

  events.push(event)

  axios.post('http://posts-clusterip-srv:4000/events', event)
  axios.post('http://comments-srv:4001/events', event)
  axios.post('http://query-srv:4002/events', event)
  axios.post('http://moderation-srv:4003/events', event)

  res.send({ status: 'OK' })
})

app.get('/events', (_, res) => {
  res.send(events)
})

const PORT = 4005
app.listen(PORT, () => {
  console.log(`> App is up and running at port ${PORT}...`)
})
