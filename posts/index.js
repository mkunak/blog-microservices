import express from 'express'
import crypto from 'crypto'
import cors from 'cors'
import axios from 'axios'

import { createRequire } from 'module' // Bring in the ability to create the 'require' method
const require = createRequire(import.meta.url); // construct the require method
const posts = require('./db/posts.json') // use the require method

const app = express()

app.use(express.json())
app.use(cors())

app.get('/posts', (_, res) => {
  res.send(posts)
})

app.post('/posts/create', async (req, res) => {
  const { title, content } = req.body
  const id = crypto.randomBytes(12).toString('hex')

  const newItem = { id, title, content }
  posts.push(newItem)

  await axios.post('http://event-bus-srv:4005/events', {
    type: 'posts:item:created',
    data: newItem,
  })

  res.status(201).send(newItem)
})

app.post('/events', (req, res) => {
  console.log('>>> Receiving event:', req.body.type)

  res.send({})
})

const PORT = 4000
app.listen(PORT, () => {
  console.log('> event-bus-srv')
  console.log(`> App is up and running at port ${PORT}...`)
})
